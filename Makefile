include .env
export

start:
	@docker-compose up -d 
	@docker-compose exec web python manage.py collectstatic
	@docker-compose exec web python manage.py migrate

stop: 
	@docker-compose down 