FROM python:3.9.2-alpine AS base 

WORKDIR /src

COPY requirements.txt .

RUN apk add --no-cache --virtual .build-deps postgresql-libs postgresql-dev gcc musl-dev && \
    pip install -r requirements.txt


FROM base

ENV PYTHONUNBUFFERED 1
EXPOSE 8000

WORKDIR /src

ADD . .

CMD ["gunicorn", "--bind", ":8000", "--workers", "1", "wsgi:application"]